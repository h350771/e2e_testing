package com.logmein.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.Properties;
import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AppTest 
{
	private String baseUrl ;
	
	@Before
	public void Before() {
		Properties p = new Properties();
    	InputStream inputStream = ClassLoader.getSystemResourceAsStream("setting.properties");
    	try {
    		p.load(inputStream);
    	} catch(Exception e) {
    		Assert.fail();
    	}
    	
    	baseUrl = p.getProperty("url");
	}
	
	
	
	@Test
    public void test() throws Exception {
    					
    	runInBrowser( webDriver -> {    		
    			
    		try {
    			
	    	webDriver.navigate().to("http://automationpractice.com");
	    	
	    	webDriver.findElement(By.cssSelector("a[title*='Log in']")).click();
	    	
	    	webDriver.findElement(By.cssSelector("input#email")).sendKeys("wattoott@gmail.com");
	    	
	    	webDriver.findElement(By.cssSelector("input#passwd")).sendKeys("testing");
	    	
	    	webDriver.findElement(By.cssSelector("button#SubmitLogin")).click();	    	
	    	
	    	assertTrue(webDriver.getPageSource().contains("Das Kolas"));   					
				    	
	    	webDriver.findElement(By.cssSelector("input[id='search_query_top']")).sendKeys("Printed dress");
	    	
	    	webDriver.findElement(By.cssSelector("button[name='submit_search']")).click();
	    	
	    	new WebDriverWait(webDriver, 10).until(ExpectedConditions.titleIs("Search - My Store"));	 	    	

	    	webDriver.findElement(By.cssSelector("ul.display.hidden-xs>li#list")).click();
	    	
	    	webDriver.findElement(By.cssSelector("ul.product_list.row.list>li:nth-of-type(2) a[title='Add to cart']")).click();
	    		    	
			Thread.sleep(3000);
			
	    	assertTrue(webDriver.getPageSource().contains("Product successfully added to your shopping cart"));
	    	
	    	webDriver.findElement(By.cssSelector("a[title='Proceed to checkout']")).click();
	    	
	    	webDriver.findElement(By.cssSelector("a[title='Add']")).click();
	   	    	
			Thread.sleep(3000);			
	    	
	      	Actions actions = new Actions(webDriver);
	    	actions.moveToElement(webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix")));
	    	actions.perform();
	    	
	    	webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix a[title='Proceed to checkout']")).click();
	    	
	    	webDriver.findElement(By.cssSelector("input[id=addressesAreEquals]")).click();	 
	    	
			Thread.sleep(4000);
	    	
	    	Actions actions2 = new Actions(webDriver);
	    	actions2.moveToElement(webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix")));
	    	actions2.perform();

	    	webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix button")).click();
	    	
	    	webDriver.findElement(By.cssSelector("input[id=cgv]")).click();	 
	    	
	    	Actions actions3 = new Actions(webDriver);
	    	actions3.moveToElement(webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix")));
	    	actions3.perform();

	    	webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix button")).click();
	    	
	    	String testText = webDriver.findElement(By.cssSelector("span[id=total_price]")).getText();
	    	
	    	assertEquals(testText, "$103.98");
	    	
	    	webDriver.findElement(By.cssSelector("a.cheque")).click();	    	

	    	webDriver.findElement(By.cssSelector("p.cart_navigation.clearfix button")).click();
	    	
	    	assertTrue(webDriver.getPageSource().contains("Your order on My Store is complete."));   	
	   
	    	} catch (InterruptedException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
	    	
    	});
    }	
    
    private void runInBrowser(Consumer<WebDriver> action) {
    	
    	String chromeDriverPath = FileSystems.getDefault().getPath("src/test/resources/chromedriver.exe").toString();
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
   	
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setHeadless(false);
		
    	WebDriver webDriver = new ChromeDriver(chromeOptions);
    	
    	try {
    		
    		action.accept(webDriver);
	    	
    	} finally {
    		webDriver.close();
    	}
    }
}
